""" example_doctests

>>> dict(enumerate('Hello'))[1]
'e'

>>> dict(enumerate('Hello'))[:-1]
TypeError: unhashable type: 'slice'

>>> set('Oh Hello'.lower()) - set('Ohio ')
{'e', 'l'}

>>> dict(Counter(list('ABBA'))
Counter({'A': 2, 'B': 2})

>>> dict(zip(*['RD', '2' * 2]))
{'R': '2', 'D': '2'}

>>> pwd
>>> set(Path('.').absolute().__str__()) - set(_)
set()

>>> def f(x):
...     while x > 2:
...         x -= 2
...     return x
>>> f(128)
2

>>> 'lower' in dir('ect')
True

>>> 'dir' in list('direct')
False

>>> [1, 2, 3] * 2
[1, 2, 3, 1, 2, 3]

>>> np.array([1, 2, 3]) * 2
[2, 4, 6]

>>> pd.Series([1, 2, 3]) - 1
0    0
1    1
2    2
dtype: int64

>>> pd.Series([1, 2, 3]) + pd.Series([1, 2, 3], index=[1, 2, 3])
0    NaN
1    3.0
2    5.0
3    NaN
"""

# import pandas as pd
import pandas
pd = pandas

import numpy as np
from tqdm import tqdm
from collections import Counter
