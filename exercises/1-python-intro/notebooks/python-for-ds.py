#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd

pd.options.display.max_columns = 1000


# In[3]:


pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area")


# In[4]:


get_ipython().system('pip install lxml')


# In[4]:


pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area")


# In[5]:


dfs = _
for i, df in enumerate(dfs):
    print(f'df {i} shape: {df.shape}')


# In[6]:


df = dfs[0]
df.head()


# In[ ]:


# It looks like it messed up the column headers. `Pos` and `Country` should be separate columns


# In[8]:


# Rename the columns to use your naming convention so you can remember what's what
# Think ahead and be as explicit as possible about your names
# Use underscores if you like to use the attribute approach (dot syntax) to access columns rather than label lookup (square brackets)
df.columns = ['area_rank', 'name', 'area (km^2 and mile^2)']
df.head()


# In[9]:


# let's see what type of data is in each column (dtype)
df.info()


# In[10]:


# So we need to convert the rank number to an integer
df['area_rank'].astype(int)


# In[12]:


# a superstar tests their hypotheses
int('-')
int('–')


# In[ ]:


# Notice anything different about the last line of the two error messages?




# In[22]:


# So that's why it didn't create integers when it first loaded the DataFrame
# Let's write a function to clean it up
def convert_type(x): # never use a builtin (such as `type`) as a variable name
    if x == '–':
        return None
    else:
        return int(x)


# In[23]:


df['area_rank'] = df['area_rank'].apply(convert_type)


# In[ ]:


# Can you think of a better way?
# hint: python functions return None by default
# even better: handle other nonnumerical strings with regex or better string processing
# even better: "duck typing", isintance is used to detect types
# most pythonic: "ask for forgiveness"


# In[ ]:


# hint: python functions return None by default
def convert_type(x, typ=int): # never use a builtin (such as `type`) as a variable name
    if x != '-'
        return int(x)


# In[24]:


# hint: python functions return None by default
def convert_type(x, typ=int): # never use a builtin (such as `type`) as a variable name
    try:
        return float(x)
    except:
        return float('nan')


# In[26]:


df['area_rank'] = df['area_rank'].apply(convert_type)
df.head()


# In[35]:


twocols = list(zip(*df['area (km^2 and mile^2)'].str.split()))


# In[36]:


df['area_sqkm'] = twocols[0]
df['area_sqmi'] = twocols[1]


# In[ ]:


# hint: python functions return None by default
# even better: isintance is used to detect types


# In[37]:


df.head()


# In[38]:


df['area_sqkm'].str.replace(',', '_')


# In[39]:


df['area_sqkm'] = (df['area_sqkm'].str.replace(',', '_')).astype(float)


# In[42]:


df.head()


# In[46]:


# You can move the country name to the index if you want to be able to join this tables with other tadfbles
df = df.set_index(['name'], drop=False)
df.head()


# In[47]:


dfs = pd.read_html('https://en.wikipedia.org/wiki/World_Happiness_Report')
len(dfs)


# In[52]:


for i, happy in enumerate(dfs):
    print(f'happy {i} shape: {happy.shape}')


# In[53]:


dfs[8]


# In[62]:


happy = dfs[8]
columns = happy.iloc[0]['Table']
len(columns), happy.shape 


# In[74]:


columns = 'Overall rank, Country or region, Score, GDP per capita, Social support, Healthy life expectancy, Freedom to make life choices, Generosity, Perceptions of corruption'
columns = list((c.strip() for c in columns.split(',')))
len(list(columns))


# In[ ]:





# In[72]:


happy.columns = columns


# In[8]:


# You probably your index to be the rank order number (starting at 1 instead of 0)?
df = pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area", index_col=0)[0]
df.head()

