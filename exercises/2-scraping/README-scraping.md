# Web Scraping 101

Sometimes the data you need is only available as an HTML web page.
Rather than copying and pasting the data you need you can use python to crawl the web and retrieve all the pieces of data you want.

## HTTP Protocol

There are two types of web requests `GET` and `POST`.

`GET` requests are what happens when you type a URL into the address bar of a browser for the first time.
All the the information about your request are in the URL.
For example when you first visit a search engine like [Qwant.com](qwant.com) the URL is something like `https://qwant.com`.

These are the kinds of requests you be using to scrape the web.

`POST` requests hide a lot of the communication within the HTTP protocol _header_.
So they're much more complicated to work with.

## Resources

- Beautiful Soup tutorial [on RealPython.com](https://realpython.com/beautiful-soup-web-scraper-python/)
- 
