To crawl wikipedia for pages linked from the page titled "Prosocial behavio":

```python
>>> from qary.etl import wikicrawl
>>> pages['Prosocial behavior'] = None
>>> pages = wikicrawl.walk_wikipedia(pages)
>>> pages
{'Prosocial behavior': (0, 0, <WikipediaPage 'Prosocial behavior'>)}
>>> pages = wikicrawl.walk_wikipedia(pages, depth=2)
```

You can hit ctrl-C midway through the crawling to cancel the web crawler and your pages object will have all the pages it has crawled already:

```
>>> pages
{'Prosocial behavior': (0, 0, <WikipediaPage 'Prosocial behavior'>),
 '2011 Tōhoku earthquake and tsunami': (1,
  0,
...
 'Gratitude': (1, 25, <WikipediaPage 'Gratitude'>),
 'Groupon': (1, 26, <WikipediaPage 'Groupon'>),
 'Guilt (emotion)': (1, 27, <WikipediaPage 'Guilt (emotion)'>)}
>>> len(pages)
29
>>> pages['Prosocial behavior']
(0, 0, <WikipediaPage 'Prosocial behavior'>)
>>> pages['Prosocial behavior'][-1]
<WikipediaPage 'Prosocial behavior'>
>>> pages['Prosocial behavior'][-1].content
'Prosocial behavior, or intent to benefit others, is a social behavior ...
>>> print(pages['Prosocial behavior'][-1].content)
```

You can parse all that text into words or sentences with spacy.
Here's how to use spacy to generate a sequence of tokens tagged with their part-of-speech, their word vector, and lots of other useful pieces of data.
SpaCy uses a pretrained model to give you all these tags.
Here's how to download and load the pretrained spacy model and then use it to parse a text document:

```python
>>> import spacy
>>> spacy.cli.download('en_core_web_md')
>>> nlp = spacy.load('en_core_web_md')
>>> doc = nlp(pages['Prosocial behavior'][-1].content)
>>> doc
Prosocial behavior, or intent to benefit others, is a social behavior that "benefit[s] ...

== Origin of the term ==
According to the psychology researcher C. Daniel Batson, the term ...

== Reciprocity vs. altruism in motivation ==
The purest forms of prosocial behavior are motivated by altruism, ...

== External links ==
Prosocial Behavior Page, US Dept. of Health and Human Services, Administration for Children and Families
Prosocial Behavior, PsychWiki.com
Informational Report describing prosocial behaviour
NPR interview with commentary on prosocial behavior from David Sloan Wilson, Aug. 28, 2011, 7 min.
The Greater Good Institute: Research on Altruism, Empathy and Prosocial Behavior
changingminds.org http://changingminds.org/explanations/theories/prosocial_behavior.htm
The Evolution Institute, https://evolution-institute.org
```

You can iterate through a Doc object to retrieve individual tokens and the `str`s that created them:

```python
>>> tokens [tok.text for tok in doc]
>>> tokens = [tok.text for tok in doc]
>>> tokens
['Prosocial',
 'behavior',
 ',',
 'or',
 ...]
```

Here's how to create DataFrame (tabular dataset) from all those tokens:

```python
>>> import pandas as pd
>>> df = pd.DataFrame(tokens)
>>> df.head()
           0
0  Prosocial
1   behavior
2          ,
3         or
4     intent
...
```

But how do we turn this into a "supervised" learning problem.
It might be easier to think about natural language text as a time series:
Imagine the words are the prices of stocks.
Let's simulate some prices so you can see the analogy between a sequence of tokens and a sequence of prices.

```
>>> import numpy as np
>>> prices = np.random.randn(1000)
>>> prices
array([ 3.24700856e-01,  2.26864861e+00, -4.46944817e-01,  1.01552270e+00,
...
       -1.11305105e+00, -2.22658622e-01, -1.22368986e-01,  1.19761046e+00])
>>> df = pd.DataFrame(prices)
>>> df
            0
0    0.324701
1    2.268649
2   -0.446945
3    1.015523
4    1.162223
..        ...
995  1.203779
996 -1.113051
997 -0.222659
998 -0.122369
999  1.197610

[1000 rows x 1 columns]
```

Now can you see how we create a supervised learning, labeled training dataset even when we only have a single column of data?
Instead of random numbers let's create a linear trend and bias and see if that helps your mental model of text as a time series:

```
>>> prices = np.random.randn(1000) + np.arange(1000) * np.pi - np.pi * 100
>>> df = pd.DataFrame(prices)
>>> df
               0
0    -314.709720
1    -312.705524
2    -307.297772
3    -304.455354
4    -303.180485
..           ...
995  2811.587509
996  2814.893575
997  2819.521303
998  2821.888382
999  2821.882613

[1000 rows x 1 columns]
```

Maybe you realized that we could use the index, the date or day number as our independent variable or feature variable for data science:

```
>>> df['daynum'] = df.index.values
>>> df
               0  daynum
0    -314.709720       0
1    -312.705524       1
2    -307.297772       2
3    -304.455354       3
4    -303.180485       4
..           ...     ...
995  2811.587509     995
996  2814.893575     996
997  2819.521303     997
998  2821.888382     998
999  2821.882613     999

[1000 rows x 2 columns]
```

But what does a Wall Street trader really want to predict?
Do they care about the price at day 0 (Day 0 in a Linux time stamp is Jan 1, 1970)?
They only have historical data for the past.
What are they really going to use this historical data to predict?
Does relabling the columns help you see it?
Imagine you're a trader at the end of the day on Jan 1, 1970 (daynum 0), you would call that "today". 
And you be starting to think about what stocks you can buy over night before the markets open again (because traders have access to the "aftermarket").

```python
>>> df.columns = ['price_today', 'daynum']
>>> df
     price_today  daynum
0    -314.709720       0
1    -312.705524       1
2    -307.297772       2
3    -304.455354       3
4    -303.180485       4
..           ...     ...
995  2811.587509     995
996  2814.893575     996
997  2819.521303     997
998  2821.888382     998
999  2821.882613     999

[1000 rows x 2 columns]
```

So you want to shift the data so that you can predict the price tomorrow as your target variable:

```python
>>> df['price_tomorrow'] = df['price_today'].shift(1)
>>> df
     price_today  daynum  price_tomorrow
0    -314.709720       0             NaN
1    -312.705524       1     -314.709720
2    -307.297772       2     -312.705524
3    -304.455354       3     -307.297772
4    -303.180485       4     -304.455354
..           ...     ...             ...
995  2811.587509     995     2808.966509
996  2814.893575     996     2811.587509
997  2819.521303     997     2814.893575
998  2821.888382     998     2819.521303
999  2821.882613     999     2821.888382

[1000 rows x 3 columns]
```

Oops!
The price_tomorrow for day 0 should be `312.705...` not `NaN`.
We shifted the data in the wrong direction. 
So go the other way with your shift:

```python
>>> df['price_tomorrow'] = df['price_today'].shift(-1)
>>> df
     price_today  daynum  price_tomorrow
0    -314.709720       0     -312.705524
1    -312.705524       1     -307.297772
2    -307.297772       2     -304.455354
3    -304.455354       3     -303.180485
4    -303.180485       4     -298.755293
..           ...     ...             ...
995  2811.587509     995     2814.893575
996  2814.893575     996     2819.521303
997  2819.521303     997     2821.888382
998  2821.888382     998     2821.882613
999  2821.882613     999             NaN

[1000 rows x 3 columns]
```

That's better. Now we just need to drop that NaN at day 999, because we don't have a price for the real tomorrow (assuming that the real today is 999).

```python
>>> df['price_tomorrow'] = df['price_today'].shift(-1).dropna()
>>> df
     price_today  daynum  price_tomorrow
0    -314.709720       0     -312.705524
1    -312.705524       1     -307.297772
2    -307.297772       2     -304.455354
3    -304.455354       3     -303.180485
4    -303.180485       4     -298.755293
..           ...     ...             ...
995  2811.587509     995     2814.893575
996  2814.893575     996     2819.521303
997  2819.521303     997     2821.888382
998  2821.888382     998     2821.882613
999  2821.882613     999             NaN

[1000 rows x 3 columns]
```
>>> df['price_tomorrow'] = df['price_today'].shift(-1)
>>> df.dropna(inplace=True)
>>> df
     price_today  daynum  price_tomorrow
0    -314.709720       0     -312.705524
1    -312.705524       1     -307.297772
2    -307.297772       2     -304.455354
3    -304.455354       3     -303.180485
4    -303.180485       4     -298.755293
..           ...     ...             ...
994  2808.966509     994     2811.587509
995  2811.587509     995     2814.893575
996  2814.893575     996     2819.521303
997  2819.521303     997     2821.888382
998  2821.888382     998     2821.882613

[999 rows x 3 columns]
```

That's a neat trick!
And let's use that other shift direction to create lots more "features" or independent variables we can use to predict tomorrow's price.


```python
>>> df['price_yesterday'] = df['price_today'].shift(1)
>>> df
     price_today  daynum  price_tomorrow  price_yesterday
0    -314.709720       0     -312.705524              NaN
1    -312.705524       1     -307.297772      -314.709720
2    -307.297772       2     -304.455354      -312.705524
3    -304.455354       3     -303.180485      -307.297772
4    -303.180485       4     -298.755293      -304.455354
..           ...     ...             ...              ...
994  2808.966509     994     2811.587509      2805.920258
995  2811.587509     995     2814.893575      2808.966509
996  2814.893575     996     2819.521303      2811.587509
997  2819.521303     997     2821.888382      2814.893575
998  2821.888382     998     2821.882613      2819.521303

[999 rows x 4 columns]
>>> df['price_2_days_ago'] = df['price_today'].shift(2)
>>> df
     price_today  daynum  ...  price_yesterday  price_2_days_ago
0    -314.709720       0  ...              NaN               NaN
1    -312.705524       1  ...      -314.709720               NaN
2    -307.297772       2  ...      -312.705524       -314.709720
3    -304.455354       3  ...      -307.297772       -312.705524
4    -303.180485       4  ...      -304.455354       -307.297772
..           ...     ...  ...              ...               ...
994  2808.966509     994  ...      2805.920258       2801.787706
995  2811.587509     995  ...      2808.966509       2805.920258
996  2814.893575     996  ...      2811.587509       2808.966509
997  2819.521303     997  ...      2814.893575       2811.587509
998  2821.888382     998  ...      2819.521303       2814.893575

[999 rows x 5 columns]
>>> hist -o -p -f predict_time_series_for_words.hist.ipy
```
