# Webify your python

## All-in-One Frameworks

- streamlit
- dash (enterprise service available)
- fastapi

https://medium.com/elca-data-science/streamlit-vs-dash-and-beyond-an-introduction-to-web-dashboard-development-frameworks-for-python-fec59d835784

## Visualization/plots

- plotly
- bokeh

## Web frameworks

- Django
- Flask
- Svelte (javascript)
