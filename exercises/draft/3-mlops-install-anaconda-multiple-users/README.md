# Install Anaconda for Multiple Users (on Linux)

- Anaconda docs [multiuser install for Windows](https://docs.anaconda.com/anaconda/install/multi-user/#multi-user-anaconda-installation-on-linux) 
- Anaconda docs [multiuser install for Windows](https://docs.anaconda.com/anaconda/install/multi-user/#multi-user-anaconda-installation-on-windows) 
- [Anaconda Linux Download](https://www.anaconda.com/products/individual-d#linux)
- [Direct link to Anaconda3-2021.05-Linux-x86_64.sh Installer](https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh)
- Direct links to all Anaconda3 installers [repo.anaconda.com/archive/](https://repo.anaconda.com/archive/)
- [Installing Anaconda for Multiple Users on by pjptech on Medium](https://medium.com/@pjptech/installing-anaconda-for-multiple-users-650b2a6666c6)

Switch to the root user and create a new user for the anaconda application:

```bash
$ sudo su
$# adduser anaconda
```

$ wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
$ chmod +x Anaconda3*
```

```bash
$ ./Anaconda3-2021.05-Linux-x86_64.sh -h

usage: ./Anaconda3-2021.05-Linux-x86_64.sh [options]

Installs Anaconda3 2021.05

-b           run install in batch mode (without manual intervention),
             it is expected the license terms are agreed upon
-f           no error if install prefix already exists
-h           print this help message and exit
-p PREFIX    install prefix, defaults to /root/anaconda3, must not contain spaces.
-s           skip running pre/post-link/install scripts
-u           update an existing installation
-t           run package tests after installation (may install conda-build)
```

So you probably want to run the installer in batch mode `-b` so that it doesn't nag you about reading the LICENSE. 
And you also can specify the directory to install everything in as /opt/anaconda with the `-p` option.
And just in case you've already created that directory you can use the `-u` option to automatically update any existing installation at that location.
I also like to run tests, because Anaconda installations can sometimes be messed up by other things I already have installed (like virtualenv).

So here's a complete install command that should work pretty well on almost any linux-like system. 
Tests could take an hour or more to run so you probably want to comment out the `-t` testing option as it is here:

```bash
./Anaconda3-2021.05-Linux-x86_64.sh -b -f -u -p /opt/anaconda  # -t
```

Now you need to set up permissions and ownership of the `conda` app.

```bash
chown -R anaconda:anaconda /opt/anaconda 
chmod -R go-w /opt/anaconda
chmod -R go+rX /opt/anaconda
sudo adduser anaconda datascience
sudo adduser hobs datascience
# sudo adduser otheruser datascience
chmod -R g+rwX /opt/anaconda
```



