# convert between IPv4 and IPv6

## You will learn

* Hexadecimal notation
* Binary notation
* IPv4 address syntax
* IPv6 address syntax

## References

* https://routing-bits.com/2009/03/19/converting-ipv4-to-ipv6/
* https://stackoverflow.com/a/19751100/623735
