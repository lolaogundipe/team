# Knowledge You Care About

1. Read this brief explanation of [knowledge graphs](https://web.stanford.edu/class/cs520/2020/notes/What_is_a_Knowledge_Graph.html)
2. From the paper, finish this graph in a yaml file (hint you should have 6 edges total:

```yaml
edges:
    - [Albert Einstein, Germany, born in]
    - [Albert Einstein, Theoretical Physicist, kind of]
```


3. plot your graph using `yaml_graphviz.py` or one of these:

- graphviz example by @Aransil: https://github.com/redransil/plotly-dirgraph
- Plotly example by Roland Smith: https://stackoverflow.com/a/59538246/623735
- Manim docs: https://docs.manim.community/en/stable/reference/manim.mobject.graph.Graph.html
- Ploty docs: https://plotly.com/python/network-graphs/
