[Sean Fairbanks](https://github.com/pysean3) is an indie San Diego developer who launched an HR management platform in 2022.
He built it with Django and Postgres and likes to take advantage of the advanced features of both.

## Hierarchical label trees

Sean creates visualizations of [ltree](https://www.postgresql.org/docs/9.1/ltree.html) data in postgres.
Ltrees are label trees that are indexed like a graph db so they can be efficiently searched with a syntax that's much simpler and faster than graph ql.
He uses CSS to dynamically display an outline or nested list of folders based on user input and changes to the database of labels for each node in the tree.
It's like a  "Table of Contents" that automatically adjusts as the user collapses or expands elements of the tree or adds new elements of the tree.
Sean uses it for org charts.
Tangible AI uses it for searchable overviews of dialog trees.

Screenshot of an [ltree+CSS org chart](sean-css-for-dynamic-outlines-TOCs.png)

## Dynamic forms

You can dynamically add additional fields to your database if you use Django Generic Fields in your `models.py`.
Sean uses this to deal with the different kinds of data that can be stored in an ltree or org chart.
Organizations have a variety of metadata they need to store and associate with departments and divisions of their corporation.

Screenshot of a dynamicly generated [Django form](sean-css-for-dynamic-outlines-TOCs-position-attributes.png)
